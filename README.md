

# Lectures for CC422 - Computing Infrastructure


## STRUCTURE

> Each activity and main topic has a \`main.pdf\` document that discuss about the topic or activity.

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">Topic</td>
<td class="org-left">Activity</td>
</tr>


<tr>
<td class="org-left">example topic [[pdf](./01)]</td>
<td class="org-left">a</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">b</td>
</tr>


<tr>
<td class="org-left">other topic</td>
<td class="org-left">c</td>
</tr>
</tbody>
</table>

